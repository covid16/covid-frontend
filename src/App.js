import "./App.css";
import "./components/login/login";
import { Login } from "./components/login/login";
import { useDispatch, useSelector } from "react-redux";
import { Navigate, Route, Routes, useNavigate } from "react-router-dom";
import { Register } from "./components/register/register";
import { Stats } from "./components/stats/stats";
import { useEffect, useState } from "react";
import { Button, Spin } from "antd";
import { useTranslation } from "react-i18next";
import { logout } from "./app/api";

function App() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [load, setLoad] = useState(false);
  const { t, i18n } = useTranslation();

  useEffect(() => {
    const data = JSON.parse(localStorage.getItem("authData"));
    if (data) {
      dispatch({
        type: "login",
        data: data,
      });
    }
    setLoad(true);
  }, []);

  const ln = (lan) => {
    i18n.changeLanguage(lan);
  };

  const authData = useSelector((state) => state.auth);

  const _logout = (e) => {
    e.preventDefault();
    logout(dispatch, navigate);
  };

  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);

  return load ? (
    <>
      <div style={{ padding: "10px 50px" }}>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>
            <h1>Covid-19</h1>
          </div>
          <div>
            {authData?.name}{" "}
            {authData.isAuthenticated && (
              <Button onClick={_logout}>Logout</Button>
            )}{" "}
            <Button
              onClick={(e) => {
                e.preventDefault();
                ln(i18n.language === "ka" ? "en" : "ka");
              }}
            >
              {i18n.language === "ka" ? "en" : "ქა"}
            </Button>
          </div>
        </div>
        <Routes>
          <Route path="login" element={<Login />} />
          <Route path="register" element={<Register />} />
          <Route
            path="/"
            element={
              isAuthenticated ? (
                <Navigate to="dashboard" />
              ) : (
                <Navigate to={"/login"} />
              )
            }
          />
          <Route path="dashboard" element={<Stats />} />
        </Routes>
      </div>
    </>
  ) : (
    <>
      <Spin />
    </>
  );
}

export default App;
