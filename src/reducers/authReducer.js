const initialState = {
  name: "",
  email: "",
  token: "",
  isAuthenticated: false,
};
export const authReducer = (state = initialState, action) => {
  if (action.type === "login") {
    return { ...state, ...action.data };
  }
  if (action.type === "logout") {
    return { ...initialState };
  }
  return state;
};
