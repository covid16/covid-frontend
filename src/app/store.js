import { configureStore, createStore } from "@reduxjs/toolkit";

import { reducers } from "../reducers/reducers";

export default createStore(reducers);
