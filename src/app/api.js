import { axiosInstance } from "./include";

export async function login(authData, dispatch, navigate) {
  return axiosInstance
    .post("login", authData)
    .then(({ data }) => {
      data = {
        token: data.token,
        name: data.name,
        email: authData.email,
        isAuthenticated: true,
      };

      dispatch({
        type: "login",
        data: data,
      });
      localStorage.setItem("authData", JSON.stringify(data));
      navigate("/dashboard");
      return Promise.resolve(data);
    })
    .catch((data) => {
      return Promise.reject(data.response.data.message);
    });
}

export const logout = (dispatch, navigate) => {
  localStorage.removeItem("authData");
  dispatch({
    type: "logout",
  });
  navigate("/");
};
