import i18n from "i18next";
import { initReactI18next } from "react-i18next";

const resources = {
  en: {
    translation: {
      "Log in": "Log in",
      register: "Register",
    },
  },
  ka: {
    translation: {
      "Log in": "შესვლა",
      register: "რეგისტრაცია",
      Or: "ან",
      Email: "ელ. ფოსტა",
      Password: "პაროლი",
      Logout: "",
      Summary: "შეჯამება",
      Name: "სახელი",
      Code: "კოდი",
      Stats: "სტატისტიკა",
      Confirmed: "დადასტურებული",
      Recovered: "მორჩენილი",
      Death: "ლეტალური",
    },
  },
};
i18n
  .use(initReactI18next)
  .init({
    resources,
    lng: "ka",
    interpolation: {
      escapeValue: false,
    },
  });

export default i18n;
