import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";

export const Home = () => {
  const navigate = useNavigate();

  const authData = useSelector((state) => state.auth.isAuthenticated);

  if (!authData) navigate("login");

  return <div>home</div>;
};
