import { Link, useNavigate } from "react-router-dom";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { axiosInstance } from "../../app/include";
import { login } from "../../app/api";
import { Button, Form, Input } from "antd";
import { useTranslation } from "react-i18next";

export const Register = () => {
  const { t, i18n } = useTranslation();
  const [error, setError] = useState(null);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleSubmit = (e) => {
    setError(null);
    axiosInstance
      .post("register", {
        name: e.name,
        email: e.email,
        password: e.password,
        password_confirmation: e.password_confirmation,
      })
      .then((data) => {
        login(
          {
            email: e.email,
            password: e.password,
          },
          dispatch,
          navigate
        ).catch((data) => setError(data));
      })
      .catch((data) => {
        setError(data.response.data.message);
      });
  };

  return (
    <div>
      <div className="login-form">
        <h2> {t("register")}</h2>
        <Form name="normal_login" onFinish={handleSubmit}>
          <Form.Item
            name="email"
            label={t("Email")}
            rules={[{ required: true, type: "email" }]}
          >
            <Input placeholder={t("Email")} />
          </Form.Item>
          <Form.Item name="name" label={t("Name")} rules={[{ required: true }]}>
            <Input placeholder={t("Name")} />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true }]}
            label={t("Password")}
          >
            <Input type="password" placeholder={t("Password")} />
          </Form.Item>
          <Form.Item
            name="password_confirmation"
            rules={[{ required: true }]}
            label={t("Password")}
          >
            <Input type="password" placeholder={t("Password")} />
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              {t("register")}
            </Button>{" "}
            {t("Or")} <Link to="/login"> {t("Log in")}</Link>
          </Form.Item>
        </Form>
        {error || <div>{error}</div>}
      </div>
    </div>
  );
};
