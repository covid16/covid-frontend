import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { Card, Input, Spin, Table, Col, Row, Divider } from "antd";
import { axiosInstance } from "../../app/include";
import { useTranslation } from "react-i18next";

export const Stats = () => {
  const authData = useSelector((state) => state.auth);
  const [statistics, setStatistics] = useState(null);
  const [summary, setSummary] = useState(null);
  const [filter, setFilter] = useState(null);
  const [displayStatistics, setDisplayStatistics] = useState(null);
  const { t, i18n } = useTranslation();

  useEffect(() => {
    axiosInstance("stats", {
      headers: { Authorization: `Bearer ${authData.token}` },
    }).then((data) => setStatistics(data.data));

    axiosInstance("summary", {
      headers: { Authorization: `Bearer ${authData.token}` },
    }).then((data) => setSummary(data.data));
  }, []);

  useEffect(() => {
    if (statistics) {
      if (filter)
        setDisplayStatistics(
          statistics.filter((item) => {
            return (
              item.id.toString().includes(filter) ||
              item.country.code.toLowerCase().includes(filter) ||
              JSON.parse(item.country.name)[i18n.language].includes(filter) ||
              item.confirmed.toString().includes(filter)
            );
          })
        );
      else {
        setDisplayStatistics(statistics);
      }
    }
  }, [filter, statistics]);
  return statistics && summary ? (
    <div>
      <h2>{t("Summary")}</h2>
      <Row>
        <Col span={8}>
          <Card title={t("Confirmed")}>{summary.confirmed}</Card>
        </Col>
        <Col span={8}>
          <Card title={t("Recovered")}>{summary.recovered}</Card>
        </Col>
        <Col span={8}>
          <Card title={t("Death")}>{summary.death}</Card>
        </Col>
      </Row>
      <Divider />
      <h2>{t("Stats")}</h2>
      <Input
        onChange={(e) => {
          setFilter(e.target.value.toLowerCase());
        }}
        value={filter}
        placeholder="filter"
      />
      <Table
        dataSource={displayStatistics.map((item) => {
          item.name = JSON.parse(item.country.name);
          return item;
        })}
        columns={[
          {
            title: t("Name"),
            dataIndex: ["name", i18n.language],
          },
          {
            title: t("Code"),
            dataIndex: ["country", "code"],
          },
          {
            title: t("Confirmed"),
            dataIndex: "confirmed",
            sorter: (a, b) => a.confirmed - b.confirmed,
          },
          {
            title: t("Recovered"),
            dataIndex: "recovered",
            sorter: (a, b) => a.recovered - b.recovered,
          },
          {
            title: t("Death"),
            dataIndex: "death",
            sorter: (a, b) => a.death - b.death,
          },
          {
            title: "id",
            dataIndex: "id",
          },
        ]}
      ></Table>
    </div>
  ) : (
    <Spin />
  );
};
