import { Link, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useState } from "react";
import { login } from "../../app/api";
import { useTranslation } from "react-i18next";
import { Button, Form, Input } from "antd";
import { KeyOutlined, UserOutlined } from "@ant-design/icons";
import "./login.css";

export const Login = () => {
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [error, setError] = useState(null);

  const handleSubmit = (e) => {
    login(
      {
        email: e.email,
        password: e.password,
      },
      dispatch,
      navigate
    ).catch((data) => setError(data));
  };
  return (
    <div>
      <div className="login-form">
        <h2>{t("Log in")}</h2>
        <Form name="normal_login" onFinish={handleSubmit}>
          <Form.Item name="email" rules={[{ required: true, type: "email" }]}>
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder={t("Email")}
            />
          </Form.Item>
          <Form.Item name="password" rules={[{ required: true }]}>
            <Input
              prefix={<KeyOutlined className="site-form-item-icon" />}
              type="password"
              placeholder={t("Password")}
            />
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              {t("Log in")}
            </Button>{" "}
            {t("Or")} <Link to="/register">{t("register")}</Link>
          </Form.Item>
        </Form>
        {error && <div>{error}</div>}
      </div>
    </div>
  );
};
