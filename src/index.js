import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import "./app/i18n";
import { Provider } from "react-redux";
import store from "./app/store";
import "antd/dist/antd.css";
import { BrowserRouter } from "react-router-dom";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <React.StrictMode>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </React.StrictMode>
  </Provider>
);
